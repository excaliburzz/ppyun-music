package com.te.ppy.mu.mapper;

import com.te.ppy.mu.domain.User;
import org.springframework.stereotype.Repository;
/*
用户dao
* @Repository方便service层调用
* */
@Repository
public interface UserMapper {

    /**
     * 验证密码是否正确
     * */
    public int verifyPassword(String email,String password);
}
