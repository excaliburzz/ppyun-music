package com.te.ppy.mu.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
/*
* 用户
* 前后台传输实现序列化
* */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    /*主键*/
    public Integer id;
    /*账户*/
    public String name;
    /*密码*/
    public String password;
    /*电子邮箱*/
    public String email;
}
