package com.te.ppy.mu.controller;

import com.alibaba.fastjson.JSONObject;
import com.te.ppy.mu.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    /*
    * 判断是否登录成功
    * */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object loginStatus(HttpServletRequest request, HttpSession session) {
        JSONObject jsonObject = new JSONObject();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        boolean flag = userService.verifyPassword(email, password);
        if (flag) {
            jsonObject.put("code", 1);
            jsonObject.put("msg", "登录成功");
            session.setAttribute("email", email);
            return jsonObject;
        }
        jsonObject.put("code", 0);
        jsonObject.put("msg", "用户名或密码错误");
        return jsonObject;
    }
}
