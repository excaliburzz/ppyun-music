package com.te.ppy.mu.service.impl;

import com.te.ppy.mu.domain.User;
import com.te.ppy.mu.mapper.UserMapper;
import com.te.ppy.mu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
*用户user实现类
* */
@Service
public class UserServiceimpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 验证密码是否正确
     *
     * @param email
     * @param password
     */
    @Override
    public boolean verifyPassword(String email, String password) {
       return userMapper.verifyPassword(email, password)>0;

    }
}
