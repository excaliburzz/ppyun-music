package com.te.ppy.mu.service;

import com.te.ppy.mu.domain.User;
/*
*用户接口
* */
public interface UserService {
    /**
     * 验证密码是否正确
     * */
    public boolean verifyPassword(String email,String password);
}
