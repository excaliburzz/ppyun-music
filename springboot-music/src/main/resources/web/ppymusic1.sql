/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : ppymusic

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2021-07-07 01:05:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `collect_user`
-- ----------------------------
DROP TABLE IF EXISTS `collect_user`;
CREATE TABLE `collect_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `type` tinyint(1) DEFAULT NULL COMMENT '收藏类型（0歌曲1歌单）',
  `song_id` int(11) DEFAULT NULL COMMENT '歌曲id',
  `song_list_id` int(11) DEFAULT NULL COMMENT '歌单id',
  `create_time` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='收藏';

-- ----------------------------
-- Records of collect_user
-- ----------------------------

-- ----------------------------
-- Table structure for `info_user`
-- ----------------------------
DROP TABLE IF EXISTS `info_user`;
CREATE TABLE `info_user` (
  `id` int(10) NOT NULL,
  `sex` varchar(5) DEFAULT NULL,
  `nature` varchar(20) DEFAULT NULL,
  `singer_img` varchar(100) DEFAULT NULL,
  `Personal_label` varchar(500) DEFAULT NULL,
  `brief_introduction` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of info_user
-- ----------------------------
INSERT INTO `info_user` VALUES ('456', '男', '个人', 'D:\\img\\huge.jpg', '中国内地影视男演员、流行乐歌手，民盟盟员', '    胡歌，1982年9月20日出生于上海市徐汇区，中国内地影视男演员、流行乐歌手，民盟盟员 [1]  ，毕业于上海戏剧学院表演系。\r\n1996年，14岁的胡歌便成为上海教育电视台的小主持人。2005年在仙侠剧《仙剑奇侠传》中塑造了“李逍遥”一角，并演唱该剧插曲《六月的雨》《逍遥叹》。2006年8月遭受严重车祸，2007年6月复出。2009年主演仙侠剧《仙剑奇侠传三》，并演唱片尾曲《忘记时间》。2010年主演的穿越剧《神话》在央视八套播出。2011年参演历史题材电影《辛亥革命》提名大众电影百花奖“最佳新人奖”。2012年主演玄幻剧《轩辕剑之天之痕》。2013年主演两部话剧，凭借《如梦之梦》获得北京丹尼国际舞台表演艺术奖“最佳男演员奖”；2014年参演战争剧《四十九日·祭》提名上海电视节白玉兰奖“最佳男配角奖”；2015年主演的谍战剧《伪装者》、古装剧《琅琊榜》、都市剧《大好时光》相继播出，获中国电视剧飞天奖“优秀男演员提名奖”、上海电视节白玉兰奖“最佳男主角奖”、中国电视金鹰奖“观众喜爱的男演员奖”、中国金鹰电视艺术节“最具人气男演员奖”等奖项。2016年登上央视春晚演唱歌曲《相亲相爱》。2017年二度登上央视春晚演唱歌曲《在此刻》，获得共青团中央“全国向上向善好青年”崇义友善好青年称号，2018年凭借《猎场》二度提名上海电视节白玉兰奖“最佳男主角奖”。\r\n2019年，主演的刁亦男执导电影《南方车站的聚会》入围戛纳国际电影节主竞赛单元。2020年12月，2020中国品牌人物500强排名53。');
INSERT INTO `info_user` VALUES ('799', '', '乐队', 'D:\\img\\b7003af33a87e950a6f2019414385343faf2b465.jpg', '中国香港摇滚乐队,由黄家驹、黄贯中、黄家强、叶世荣组成', '    1983年参加“山叶吉他比赛”获得冠军并正式出道。1986年自资发行乐队首张专辑《再见理想》。1988年凭借粤语专辑《秘密警察》在香港乐坛获得关注；同年凭借歌曲《大地》获得十大劲歌金曲奖。1989年凭借歌曲《真的爱你》获得十大中文金曲奖、十大劲歌金曲奖。1990年凭借歌曲《光辉岁月》获得十大劲歌金曲奖。1991年主演电影《Beyond日记之莫欺少年穷》；同年9月在香港红磡体育馆举办“Beyond Live 1991 生命接触演唱会”。1992年赴日本发展演艺事业。1993年凭借粤语专辑《乐与怒》中的歌曲《海阔天空》获得十大中文金曲奖；6月30日，黄家驹去世，乐队以三名成员的组成形式继续发展。\r\n1994年发行专辑《二楼后座》。1995年至1999年发行的《Sound》《请将手放开》《不见不散》等五张专辑标志着乐队音乐风格的转变。1996年起连续四年获得叱咤乐坛流行榜叱咤乐坛组合金奖。1999年乐队宣布暂时解散。2002年获得第25届十大中文金曲金曲银禧荣誉大奖。2003年正式复出乐坛，并举办“Beyond超越Beyond”世界巡回演唱会。2004年凭借歌曲《长空》获得第23届香港电影金像奖最佳原创电影歌曲奖。2005年在举行“Beyond The Story Live 2005”世界巡回告别演唱会后宣布正式解散。2010年获得华语金曲奖“30年经典评选”的全部奖项。\r\n演艺事业外，Beyond热心公益慈善。1991年前往非洲探访第三世界的穷困人民，并成立第三世界基金');
INSERT INTO `info_user` VALUES ('1154', '男', '个人', 'D:\\img\\b510c83b1cca3234.jpg', '华语流行乐男歌手、音乐人、潮牌主理人', '    2003年发行首张创作专辑《乐行者》，并于次年凭借该专辑获得第15届台湾金曲奖最佳新人奖。2004年凭借专辑《第二天堂》中的主打歌《江南》获得广泛关注。2006年首次举办个人巡演“就是俊杰”世界巡回演唱会。2014年凭借专辑《因你而在》获得第25届台湾金曲奖最佳国语男歌手奖 。2016年凭借专辑《和自己对话》和歌曲《不为谁而作的歌》分别获得第27届台湾金曲奖最佳国语男歌手奖、最佳作曲人奖， 并推出个人首部音乐纪录片《听·见林俊杰》。2020年推出双维度EP专辑《幸存者·如你》。截至2020年，林俊杰已发行14张正式专辑，累计创作数百首歌曲。\r\n2007年成立个人音乐制作公司JFJ Productions。2008年创立潮流品牌SMG。2016年获得国际汽车联盟（FIA）职业赛车执照。2017年成立“SMG”电竞战队。\r\n演艺事业外，林俊杰热心社会活动。2004年、2015年两度受邀演唱新加坡国庆庆典主题曲。2008年担任北京奥运会火炬手；同年为5·12汶川地震创作并演唱歌曲《爱与希望》。2010年担任新加坡青奥会 火炬手；同年创作并演唱上海世博会新加坡馆主题曲《感动每一刻》。凭借在音乐创作与慈善公益事业等方面的表现，2009年荣获新加坡杰出青年奖 ，2014年获得第5届通商中国青年奖 ');
INSERT INTO `info_user` VALUES ('2223', '男', '个人', 'D:\\img\\src=http___wx4.sirjt8.jpg&refer=http___wx4.sinaimg.jpg', '中国香港男歌手、演员、音乐人，影视歌多栖发展的代表之一', '    张国荣（1956年9月12日-2003年4月1日），出生于香港，中国香港男歌手、演员、音乐人，影视歌多栖发展的代表之一 。\r\n1977年出道。1983年以《风继续吹》获得关注。1984年演唱的《Monica》是香港歌坛第一支同获十大中文金曲、十大劲歌金曲的舞曲。1986年、1987年凭《有谁共鸣》《无心睡眠》获劲歌金曲金奖；之后凭专辑《爱慕》《The Greatest Hits of Leslie Cheung》成为首位打入韩国音乐市场的粤语歌手，并打破华语唱片在韩国的销量纪录。1988年、1989年获十大劲歌金曲最受欢迎男歌星奖。1999年获香港乐坛最高荣誉金针奖。2000年获CCTV-MTV音乐盛典亚洲最杰出艺人奖。2010年入选美国CNN“过去50年里全球最知名的20位歌手/乐团”。他擅长词曲创作，担任过MV导演、唱片监制、演唱会艺术总监等。\r\n1978年开始参演电视剧。80年代中期将事业重心移至影坛，塑造了宁采臣、旭仔、程蝶衣、欧阳锋、何宝荣等角色。1991年凭《阿飞正传》获金像奖最佳男主角奖。1993年主演的《霸王别姬》是中国电影史上首部获得戛纳国际电影节金棕榈大奖的电影 ，并打破中国内地文艺片在美国的票房纪录，他凭此片受到国际影坛的关注，获日本影评人大奖最佳男主角、中国电影表演艺术学会奖特别贡献奖；同年任东京国际电影节评委。1998年成为首位担任柏林国际电影节评委的亚洲男演员。2000年入选《日本电影旬报》20世纪百大外国男演员。2005年入选中国电影百年百位优秀演员。2010年入选美国CNN“史上最伟大的25位亚洲演员”。');
INSERT INTO `info_user` VALUES ('2468', '男', '个人', 'D:\\img\\src=http___imgcac___imgcache.cjmx.jpg', '中国内地流行乐男歌手、音乐人、演员', '    2010年，发行个人首张音乐EP《小黄》。2013年，发行个人首张音乐专辑《模特》，凭该专辑成为首位获得台湾金曲奖“最佳新人奖”的中国大陆歌手。2014年，发行同名音乐专辑《李荣浩》，凭该专辑获得中国TOP排行榜\"年度内地最佳男歌手奖\"。2016年，发行音乐专辑《有理想》，随后，举行的23场“有理想”世界巡演获得第21届华语榜中榜\"亚洲影响力最佳演唱会奖\"。2017年，发行个人音乐专辑《嗯》，凭该专辑获得第22届全球华语榜中榜亚洲影响力最佳男歌手奖；同年，他的个人巡演首次登陆香港体育馆和台北小巨蛋体育馆。2018年，发行音乐专辑《耳朵》，凭该专辑提名第30届台湾金曲奖最佳国语男歌手奖。2019年，担任浙江卫视歌唱选秀节目《中国好声音》的导师；同年，获得Mnet亚洲音乐大奖\"亚洲最佳艺人奖\"。2020年，发行音乐专辑《麻雀》');
INSERT INTO `info_user` VALUES ('3155', '男', '个人', 'D:\\img\\src=http___n.sinaimg.cn_sinacn08__n.sinaimg.jpg', '华语流行乐男歌手、音乐人、影视演员、导演', '    1995年发行个人首张专辑《情敌贝多芬》，从而进入演艺圈。1998年凭借专辑《公转自转》获得关注，并于次年凭借该专辑获得第10届台湾金曲奖最佳国语男歌手奖、最佳唱片制作人奖。2004年凭借专辑《不可思议》获得第15届台湾金曲奖最佳唱片制作人奖。2005年在专辑《心中的日月》中首创Chinked-out曲风。2006年凭借专辑《盖世英雄》获得第17届台湾金曲奖最佳国语男歌手奖。2008年被美国网站GOLDSEA评选为“80位美国最有影响力的美籍亚洲人”之一。2017年主演音乐纪录片《火力全开》。\r\n2000年以动作电影《雷霆战警》涉足影坛。2007年出演剧情片《色·戒》。2010年自编自导自演爱情电影《恋爱通告》。2013年出演好莱坞电影《骇客交锋》并签约CAA。2018年凭借青春爱情片《无问西东》获得第10届澳门国际电影节最佳男主角奖。\r\n演艺事业外，王力宏积极参与社会活动。2002年担任联合国儿童基金会国际青年大使。2003年创作公益歌曲《手牵手》。2008年担任北京奥运会火炬手。2010年担任上海世博会台北馆形象大使。2011年与前台湾领导人马英九在台湾清华大学进行对谈。2012年担任伦敦奥运会火炬手。2013年受邀在牛津大学进行一场以“认识华流”为主题的演讲。2019年在武汉军运会开幕式上演唱主题歌《和平的薪火》》');
INSERT INTO `info_user` VALUES ('4397', '男', '个人', 'D:\\img\\T001R300x300M000002azErJ0UcDN6.jpg', '中国流行男歌手', '    2004年参加歌唱类选秀《我型我秀》，获得全国总冠军并出道。2007\r\r\n年参加歌唱类选秀《快乐男声》，获得总决赛第四名；随后发行的EP《最\r\r\n美的太阳》拿下亚马逊年度唱片销量冠军。2008年发行专辑《明天过后》\r\r\n，凭借该专辑获得北京流行音乐典礼11项提名。2010年发行专辑《这，就\r\r\n是爱》；同年获得韩国MAMA亚洲最佳歌手奖。2012年在人民大会堂开启个\r\r\n人首轮巡演； 同年获得湖南省“五个一”工程奖。2013年首次参加央视\r\r\n春晚，演唱歌曲《给我你的爱》。\r\n    2014年，获得第42届全美音乐奖年度国际艺人奖，成为首位获得此奖\r\r\n的华人歌手。2016年开启张杰我想世界巡回演唱会，覆盖亚、欧、美、澳\r\r\n四个大洲，并成为首位在欧洲开个唱的内地歌手。2017年获得中国金唱片\r\r\n奖最佳流行歌手奖；2018年献唱建军91周年主题曲《微笑着胜利》；同年\r\r\n开启的张杰未·LIVE巡回演唱会两次开唱北京鸟巢，并且两度刷新鸟巢单\r\r\n场演唱会票房纪录；随后发行的MIX-POP专辑《未·LIVE》销量打破IFPI\r\r\n三白金认证。2019年他的巡演人数成为年度歌手巡演总人次内地冠军2020\r\r\n年发行英文专辑《Risk It All声来无畏》。张杰热心公益慈善业。2012\r\r\n年创立北斗星空爱心基金，截止2020年在中国各地已建立近50间杰音乐梦\r\r\n想教室，在中国西部地区搭建了数十所公益小学。');
INSERT INTO `info_user` VALUES ('7422', '男', '个人', 'D:\\img\\20200420161323164.jpg', '中国内地男歌手、音乐创作人，摇滚乐队“鲍家街43号”的发起人', '    汪峰自幼在中央音乐学院附小、附中学习小提琴，大学考入中央音乐学院小提琴中提琴专业，大学期间在专业音乐学习和训练之余就开始进行摇滚乐创作并组建乐队。完成本科学业后，进入中央芭蕾舞团任副首席小提琴师，后辞职转型为职业歌手。\r\n    1994年组建“鲍家街43号”乐队并担任主唱。1997年、1998年随乐队发行《鲍家街43号》《风暴来临》两张专辑。2000年开始个人发展，并发表第一张个人专辑《花火》。此后发行《爱是一颗幸福的子弹》《勇敢的心》《信仰在空中飘扬》《生无所求》《生来彷徨》《果岭里29号》《2020》等个人专辑，创作并演唱了《在雨中》《怒放的生命》《飞得更高》《春天里》《北京北京》《我爱你中国》等百余首歌曲。\r\n    2012年4月，小说《晚安北京》问世。2013年6月加盟《中国好声音第二季》担任导师并留任第三季。2013年12月2日发行专辑《生来彷徨》。2014年开展“汪峰2014峰暴来临演唱会”超级巡演。2017年8月15日，汪峰首部音乐电影《存在EXISTENCE》上线，首播当日播放量便突破百万。');
INSERT INTO `info_user` VALUES ('7958', '女', '个人', 'D:\\img\\20180731105329370.jpg', '中国流行乐女歌手、影视演员 [1]  ，中国国家一级演员 ', '    1989年，凭借粤语歌曲《仍是旧句子》在香港乐坛出道；同年发行首张个人专辑《王靖雯》。1992年，因演唱歌曲《容易受伤的女人》而被听众所熟知。1993年，在粤语专辑《十万个为什么？》 中尝试另类音乐的风格。1994年，发行普通话专辑《天空》，并获得十大劲歌金曲最受欢迎女歌星奖；同年在香港红磡体育馆举办18场“最精彩演唱会”，打破中国香港歌手初次开演唱会的场次纪录。1996年，成为首位登上美国《时代周刊》封面的华人歌手。1998年，因在央视春晚上演唱歌曲《相约一九九八》而在中国内地获得更多的关注；同年发行专辑《唱游》，通过运用颤音、拖音等技巧确立其个人风格，并于次年获得新加坡SPVA唱片畅销排行榜年度销量冠军。\r\n    1999年，凭借英文歌曲《Eyes on Me》获得第41届日本唱片大奖亚洲音乐奖，同年在日本武道馆举行个人演唱会。2002年，获得吉尼斯世界纪录大全颁发的“唱片销量最高的粤语女歌手”证书。2003年主演喜剧电影《天下无双》。2004年，凭借专辑《将爱》获得第15届台湾金曲奖最佳国语女演唱人奖。2005年，在结束“菲比寻常巡回演唱会”后淡出乐坛 。2010年，复出乐坛并在央视春晚上演唱歌曲《传奇》，同年举办巡回演唱会。2018年，与那英在春晚演唱《岁月》。\r\n    演艺事业外，王菲热心公益慈善。2006年11月，成立嫣然基金会。2012年5月，成为北京嫣然天使儿童医院的创始人之一');
INSERT INTO `info_user` VALUES ('8131', '女', '个人', 'D:\\img\\65a92dc9ly8ggomvp6ywsj20e80e874s.jpg', '中国香港流行乐女歌手、词曲作者、音乐制作人', '    2008年，发行个人首张音乐EP《G.E.M.》，凭该EP获得香港叱咤乐坛流行榜“叱咤乐坛生力军女歌手（金奖）”。2009年，发行个人首张音乐专辑《18...》。2011年，成为首位登上香港体育馆开唱的90后华语女歌手。2012年，凭借音乐专辑《Xposed》入围第24届台湾金曲奖“最佳国语女歌手奖”，并获得IFPI香港唱片销量大奖“全年最高销量女歌手奖”和“最高销量国语唱片奖”。2013年，举行邓紫棋“X.X.X.”世界巡回演唱会。2014年，获得湖南卫视歌唱真人秀节目《我是歌手第二季》总决赛亚军；同年，获得第27届美国儿童选择奖“最受欢迎亚洲艺人奖”。\r\n    2015年，登上2015年中央电视台春节联欢晚会，弹唱歌曲《多远都要在一起》；同年，位列《2015年福布斯中国名人榜》第11名。2016年，入选福布斯《全球30岁以下最具潜力音乐人榜单》；同年，获得MTV欧洲音乐奖“中国内地及香港地区最佳艺人奖” 。2018年，成为美国国家航空航天局“科学突破奖”首位亚洲颁奖女艺人 ；同年，入选英国广播公司BBC发布的《全球最具影响力百大女性榜单》。2019年，《光年之外》MV在视频平台YouTube突破2亿次点击量。2020年，凭借音乐专辑《摩天动物园》获得第31届台湾金曲奖“评审团奖”；同年，获得Mnet亚洲音乐大奖“最佳亚洲艺人奖”');
INSERT INTO `info_user` VALUES ('9453', '女', '个人', 'D:\\img\\67dfe971ly8g5n0cirqp8j20ry0ryjvv.jpg', '华语流行乐女歌手、舞者、服装设计师', '    1998年参加MTV音乐台举办的“新生卡位战”音乐比赛并夺得冠军。1999年7月发行个人首支单曲EP《和世界做邻居》，并从此开启演唱生涯；同年9月发行个人首张专辑《Jolin 1019》正式出道。2001年获得MTV音乐录影带大奖台湾观众选择奖。2002年以“Mando-Pop”身份登上《商业周刊》美国版正刊封面；同年发表个人转型复出之作《骑士精神》。2003年发行专辑《看我72变》。2005年发行个人第七张普通话专辑《J-game》，该专辑在全亚洲销量超过100万张。2006年推出专辑《舞娘》，并凭借该专辑获得第18届台湾金曲奖最佳国语女歌手奖、MTV亚洲音乐大奖国际时尚艺人奖。2007年发行专辑《特务J》。2008年凭借歌曲《Love Love love》登上荷兰Xtips排行榜冠军。2009年发行专辑《花蝴蝶》。2010年推出概念专辑《Myself》；同年举行“MYSELF”世界巡回演唱会。\r\n    2012年发行专辑《MUSE》，凭借《大艺术家》获得第24届台湾金曲奖最佳年度歌曲、提名第19届MTV欧洲音乐录影带大奖最佳亚洲艺人。2014年发行第13张专辑《呸》，并凭借该专辑获得第26届台湾金曲奖最佳国语专辑奖。2015年举行蔡依林“Play”世界巡回演唱会；同年获得MAMA亚洲最佳艺人奖 。2018年推出暌违乐坛四年的专辑《UGLY BEAUTY》，并在次年凭借该专辑获得第30届金曲奖年度专辑奖');

-- ----------------------------
-- Table structure for `list_song`
-- ----------------------------
DROP TABLE IF EXISTS `list_song`;
CREATE TABLE `list_song` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `song_id` int(11) DEFAULT NULL COMMENT '歌曲id',
  `song_list_id` int(11) DEFAULT NULL COMMENT '歌单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8 COMMENT='歌单包含歌曲列表';

-- ----------------------------
-- Records of list_song
-- ----------------------------

-- ----------------------------
-- Table structure for `rank_user`
-- ----------------------------
DROP TABLE IF EXISTS `rank_user`;
CREATE TABLE `rank_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `type` tinyint(1) DEFAULT NULL COMMENT '评论类型（0歌曲1歌单）',
  `song_id` int(11) DEFAULT NULL COMMENT '歌曲id',
  `song_list_id` int(11) DEFAULT NULL COMMENT '歌单id',
  `content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `create_time` datetime DEFAULT NULL COMMENT '收藏时间',
  `up` int(11) DEFAULT '0' COMMENT '评论点赞数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='评论';

-- ----------------------------
-- Records of rank_user
-- ----------------------------

-- ----------------------------
-- Table structure for `singer_user`
-- ----------------------------
DROP TABLE IF EXISTS `singer_user`;
CREATE TABLE `singer_user` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL COMMENT '头像',
  `introduction` varchar(255) DEFAULT NULL COMMENT '简介',
  PRIMARY KEY (`id`),
  CONSTRAINT `gid` FOREIGN KEY (`id`) REFERENCES `info_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='歌手';

-- ----------------------------
-- Records of singer_user
-- ----------------------------
INSERT INTO `singer_user` VALUES ('456', '胡歌', null, null, null);
INSERT INTO `singer_user` VALUES ('799', 'Beyond', null, null, null);
INSERT INTO `singer_user` VALUES ('1154', '林俊杰', null, null, null);
INSERT INTO `singer_user` VALUES ('2223', '张国荣', null, null, null);
INSERT INTO `singer_user` VALUES ('2468', '李荣浩', null, null, null);
INSERT INTO `singer_user` VALUES ('3155', '王力宏', null, null, null);
INSERT INTO `singer_user` VALUES ('4397', '张杰', null, null, null);
INSERT INTO `singer_user` VALUES ('7422', '汪峰', null, null, null);
INSERT INTO `singer_user` VALUES ('7958', '王菲', null, null, null);
INSERT INTO `singer_user` VALUES ('8131', 'G.E.M邓紫棋', null, null, null);
INSERT INTO `singer_user` VALUES ('9453', '蔡依林', null, null, null);

-- ----------------------------
-- Table structure for `song_list_user`
-- ----------------------------
DROP TABLE IF EXISTS `song_list_user`;
CREATE TABLE `song_list_user` (
  `id` int(20) NOT NULL,
  `style` varchar(255) DEFAULT NULL COMMENT '流派',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `img` varchar(255) DEFAULT NULL COMMENT '图片',
  `introduction` varchar(255) DEFAULT NULL COMMENT '简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='歌单';

-- ----------------------------
-- Records of song_list_user
-- ----------------------------
INSERT INTO `song_list_user` VALUES ('1', '华语', null, null, null);
INSERT INTO `song_list_user` VALUES ('2', '流行', null, null, null);
INSERT INTO `song_list_user` VALUES ('3', '摇滚', null, null, null);
INSERT INTO `song_list_user` VALUES ('4', '民谣', null, null, null);
INSERT INTO `song_list_user` VALUES ('5', '电子', null, null, null);
INSERT INTO `song_list_user` VALUES ('6', '轻音乐', null, null, null);
INSERT INTO `song_list_user` VALUES ('7', '乡村', null, null, null);
INSERT INTO `song_list_user` VALUES ('8', '说唱', null, null, null);
INSERT INTO `song_list_user` VALUES ('9', '古典', null, null, null);
INSERT INTO `song_list_user` VALUES ('10', '爵士', null, null, null);
INSERT INTO `song_list_user` VALUES ('11', '蓝调', null, null, null);
INSERT INTO `song_list_user` VALUES ('12', '金属', null, null, null);
INSERT INTO `song_list_user` VALUES ('13', '古风', null, null, null);
INSERT INTO `song_list_user` VALUES ('14', '拉丁', null, null, null);
INSERT INTO `song_list_user` VALUES ('15', '英伦', null, null, null);
INSERT INTO `song_list_user` VALUES ('16', '舞曲', null, null, null);

-- ----------------------------
-- Table structure for `song_user`
-- ----------------------------
DROP TABLE IF EXISTS `song_user`;
CREATE TABLE `song_user` (
  `song_id` int(10) NOT NULL,
  `singer_id` int(10) NOT NULL COMMENT '歌手id',
  `song_name` varchar(40) DEFAULT '' COMMENT '歌曲名字',
  `introduction` varchar(255) DEFAULT NULL COMMENT '歌曲简介',
  `schools` int(20) DEFAULT NULL COMMENT '歌曲分类',
  `song_address` varchar(255) DEFAULT NULL COMMENT '歌曲地址',
  `img` varchar(255) DEFAULT NULL COMMENT '歌曲图片',
  `lyric` text COMMENT '歌词',
  PRIMARY KEY (`song_id`,`singer_id`),
  KEY `singer_id` (`song_id`),
  KEY `schools` (`schools`),
  KEY `ujhb` (`singer_id`),
  CONSTRAINT `gdgd` FOREIGN KEY (`schools`) REFERENCES `song_list_user` (`id`),
  CONSTRAINT `ujhb` FOREIGN KEY (`singer_id`) REFERENCES `singer_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='歌曲表';

-- ----------------------------
-- Records of song_user
-- ----------------------------
INSERT INTO `song_user` VALUES ('4561', '456', '逍遥叹', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\胡歌-逍遥叹.mp3', '13', null, null, null);
INSERT INTO `song_user` VALUES ('4562', '456', '忘记时间', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\胡歌-忘记时间.mp3', '2', null, null, null);
INSERT INTO `song_user` VALUES ('7991', '799', '海阔天空', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\海阔天空.mp3', '3', null, null, null);
INSERT INTO `song_user` VALUES ('7992', '799', '光辉岁月', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\Beyond-光辉岁月.mp3', '3', null, null, null);
INSERT INTO `song_user` VALUES ('7993', '799', '真的爱你', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\Beyond-真的爱你.mp3', '1', null, null, null);
INSERT INTO `song_user` VALUES ('7994', '799', '无悔这一生', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\Beyond-无悔这一生.mp3', '1', null, null, null);
INSERT INTO `song_user` VALUES ('79581', '7958', '红豆', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\王菲-红豆.mp3', '1', null, null, null);
INSERT INTO `song_user` VALUES ('79582', '7958', '匆匆那年', 'C:\\Users\\Administrator\\Desktop\\share\\data\\song\\王菲-匆匆那年.mp3', '2', null, null, null);
INSERT INTO `song_user` VALUES ('94531', '9453', '今天你要嫁给我', '', '2', null, null, null);
INSERT INTO `song_user` VALUES ('94532', '9453', '倒带', '', '2', null, null, null);
INSERT INTO `song_user` VALUES ('94533', '9453', '日不落', '', '2', null, null, null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT '主键',
  `username` varchar(255) DEFAULT '' COMMENT '账号',
  `password` varchar(255) DEFAULT '' COMMENT '密码',
  `email` char(30) DEFAULT '' COMMENT '邮件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表\r\n';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('0', '', '', '');
